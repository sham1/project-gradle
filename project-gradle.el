;;; project-gradle.el --- gradle support for project.el  -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Jani Juhani Sinervo

;; Version: 0.1.1
;; Author: Jani Juhani Sinervo <jani@sinervo.fi>
;; URL: https://codeberg.org/sham1/project-gradle
;; Package-Requires: ((emacs "28.2") (project "0.3.0"))

;;; Commentary:

;; This package provides a project.el backend for dealing with Gradle-based projects.

;;; Code:

(require 'find-func)
(require 'compile)
(require 'cl-lib)
(require 'project)

(defconst project-gradle--project-pathfinder (expand-file-name "pathfinder/app/build/install/app/bin/app" (file-name-directory load-file-name))
  "Location of the project-gradle pathfinder program.")

(defun project-gradle-build-pathfinder ()
  "Compiles the gradle pathfinder if not already compiled."
  (interactive)
  (unless (file-exists-p project-gradle--project-pathfinder)
    (let* ((gradle-wrapper (if (eq system-type 'windows-nt) "gradlew.bat" "./gradlew"))
	   (build-command (format "%s install " gradle-wrapper)))
      (let ((default-directory (expand-file-name "pathfinder" (file-name-directory (find-library-name "project-gradle")))))
	(compilation-start build-command nil
			   #'(lambda (&optional ignored)
			       "*project-gradle pathfinder build*"))))))

(defun project-gradle-try (dir)
  "Try to find the dominating gradle project relative to DIR."
  (let ((dominator (locate-dominating-file dir ".gradle")))
    (when dominator
      (let ((default-directory dominator))
	(when (file-exists-p project-gradle--project-pathfinder)
	  (let ((lines nil))
            (with-temp-buffer
	      (call-process project-gradle--project-pathfinder 'nil 't 'nil)
	      (goto-char (point-min))
	      (while (not (eobp))
		(let* ((line-start (line-beginning-position))
		       (line-end (line-end-position))
		       (line (buffer-substring-no-properties line-start line-end)))
		  (push line lines)
		  (forward-line 1)))
	      (setq lines (nreverse lines)))
	    (when lines
	      (let ((root (car lines))
		    (extra-roots (cdr lines)))
		(list 'gradle root extra-roots)))))))))

(cl-defmethod project-root ((project (head gradle)))
  "Find the root of the PROJECT based on gradle."
  (cl-second project))

(cl-defmethod project-external-roots ((project (head gradle)))
  "Find the external roots of the PROJECT based on gradle."
  (cl-third project))

;;;###autoload
(add-to-list 'project-find-functions #'project-gradle-try)

(provide 'project-gradle)
;;; project-gradle.el ends here
