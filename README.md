# project-gradle

This simple package provides the ability to use gradle-based projects within the project.el ecosystem.

## Usage

When loading the library, one must also build the "pathfinder"-tool.
To do this, one may call the following after having loaded the package:

```elisp
(project-gradle-build-pathfinder)
```

NOTE: One must have a Java development environment installed for this to work,
and thus this should only be done conditionally.

## License

GPLv3+
