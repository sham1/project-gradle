package xyz.sham1.projectgradle;

import java.io.File;

import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;
import org.gradle.tooling.model.gradle.BasicGradleProject;
import org.gradle.tooling.model.gradle.GradleBuild;

public class App {
    public static void main(String[] args) {
	String currentDirectory = System.getProperty("user.dir");
        try (ProjectConnection connection = GradleConnector.newConnector()
	     .forProjectDirectory(new File(currentDirectory))
	     .connect()) {
	    GradleBuild build = connection.getModel(GradleBuild.class);
	    BasicGradleProject rootProject = build.getRootProject();
	    System.out.println(rootProject.getProjectDirectory());
	    for (BasicGradleProject subProject : rootProject.getChildren()) {
		System.out.println(subProject.getProjectDirectory());
	    }
	}
    }
}
